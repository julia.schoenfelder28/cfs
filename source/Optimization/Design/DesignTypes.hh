#ifndef DESIGNTYPES_HH_
#define DESIGNTYPES_HH_


/** excluded type definitions to prevent circular includes */

namespace CoupledField {

/** required in DesignSpace and DesignMaterial */

} // end of namespace

#endif /* DESIGNTYPES_HH_ */
